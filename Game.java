import javax.swing.*;
import java.awt.*;


public class Game{

    private JFrame frame = new JFrame();
    JLabel[] mainmenu = new JLabel[4];
    JPanel pane = new JPanel();

    public Game(){
        frame.setTitle("Star Lighthouse");
        frame.setSize(1280,720);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        pane.setLayout(null);


        this.showTitleScreen();

        frame.setContentPane(pane);
        frame.setVisible(true); //must be at the end
    }

    public void showTitleScreen(){
        mainmenu[0] = new JLabel(new ImageIcon(this.getClass().getResource("assets/Titlebg.gif")));
        mainmenu[0].setLocation(0, 0);
        mainmenu[1] = new JLabel(new ImageIcon(this.getClass().getResource("assets/Logo.png")));
        mainmenu[1].setLocation(50, 200);
        mainmenu[2] = new JLabel(new ImageIcon(this.getClass().getResource("assets/newgame.png")));
        mainmenu[2].setLocation(50, 400);
        mainmenu[3] = new JLabel(new ImageIcon(this.getClass().getResource("assets/loadgame.png")));
        mainmenu[3].setLocation(50, 500);

        for(int i=0; i<4; i++){
            pane.add(mainmenu[0]);
        }

    }
}